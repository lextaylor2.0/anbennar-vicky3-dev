﻿COUNTRIES = {
	c:A09 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism	#busilar exports colonial goods
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_resettlement
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_tenant_farmers

		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_same_heritage_only # human supremacy
		
		
		activate_law = law_type:law_nation_of_artifice	#ravelian, after all
		
		ig:ig_devout = {
			add_ruling_interest_group = yes
		}

	}
}