﻿POPS = {
	s:STATE_ROILSARD = {
		region_state:A03 = {
			create_pop = {
				culture = roilsardi
				size = 3312976
			}
			create_pop = {
				culture = redglader_elf
				size = 11458
				religion = corinite
			}
		}
	}

	s:STATE_ROSECROWN = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 2811406
			}
			create_pop = {
				culture = redfoot_halfling
				size = 562979
				split_religion = {
					redfoot_halfling = {
						regent_court = 0.7
						corinite = 0.3
					}
				}
			}
			create_pop = {
				culture = ruby_dwarf
				size = 280323
			}
			create_pop = {
				culture = redglader_elf
				size = 177456
				religion = regent_court
			}
			create_pop = {
				culture = creek_gnome
				size = 42621
			}
		}
	}

	s:STATE_BLOODWINE = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 2476995
			}
			create_pop = {
				culture = redfoot_halfling
				size = 450193
				religion = regent_court
			}
			create_pop = {
				culture = redglader_elf
				size = 147992
				religion = regent_court
			}
			create_pop = {
				culture = ruby_dwarf
				size = 33889
			}
		}
	}

	s:STATE_GREAT_ORDING = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 2120126
			}
			create_pop = {
				culture = derannic
				size = 117474
			}
		}
	}

	s:STATE_WINEBAY = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1129762
				split_religion = {
					lorentish = {
						ravelian = 0.2	#growing
						regent_court = 0.8
					}
				}
			}
			create_pop = {
				culture = sorncosti
				size = 316254
				split_religion = {
					sorncosti = {
						ravelian = 0.2	#growing
						regent_court = 0.8
					}
				}
			}
			create_pop = {
				culture = redfoot_halfling
				size = 271179
				religion = regent_court
			}
			create_pop = {
				culture = redglader_elf
				size = 76280
				religion = regent_court
			}
			create_pop = {
				culture = ruby_dwarf
				size = 31416
			}
		}
	}

	s:STATE_SORNCOST = {
		region_state:A03 = {
			create_pop = {
				culture = sorncosti
				size = 2322567
			}
			create_pop = {
				culture = ruby_dwarf
				size = 48472
			}
			create_pop = {
				culture = redglader_elf
				size = 28555
				religion = regent_court
			}
		}
	}


	s:STATE_VENAIL = {
		region_state:A03 = {
			create_pop = {
				culture = redglader_elf
				size = 106764
				religion = regent_court
			}
			create_pop = {
				culture = sorncosti
				size = 641579
			}
		}
	}

	s:STATE_RUBYHOLD = {
		region_state:A75 = {
			create_pop = {
				culture = ruby_dwarf
				size = 2549742
			}
			create_pop = {
				culture = lorentish
				size = 231062
			}
			create_pop = {
				culture = exwesser
				size = 142930
			}
			create_pop = {
				culture = costine
				size = 54799
			}
		}
	}

	s:STATE_RUBY_MOUNTAINS = {
		region_state:A75 = {
			create_pop = {
				culture = ruby_dwarf
				size = 923186
			}
			#These dudes are shepherds living in the mountain valleys above the mines
			create_pop = {
				culture = lorentish
				size = 35814
			}
			create_pop = {
				culture = exwesser
				size = 19531
			}
		}
	}

	s:STATE_DAROM = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1965057
			}
			create_pop = {
				culture = derannic
				size = 429773
			}
		}
	}

	s:STATE_DERANNE = {
		region_state:A03 = {
			create_pop = {
				culture = derannic
				size = 2898586
				split_religion = {
					derannic = {
						corinite = 0.2	#resistance. heavily presecuted
						regent_court = 0.8
					}
				}
			}
			create_pop = {
				culture = lorentish
				size = 157940
			}
			create_pop = {
				culture = creek_gnome
				size = 21396
			}
			create_pop = {
				culture = redglader_elf	#Aelvar elves
				size = 6848
				religion = regent_court
			}
		}
	}

	s:STATE_REDGLADES = {
		region_state:A65 = {
			create_pop = {
				culture = redglader_elf
				size = 507360
				religion = regent_court
			}
		}
	}

	s:STATE_SOUTHROY = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 962798
			}
			create_pop = {
				culture = iochander
				size = 376747
			}
			create_pop = {
				culture = redfoot_halfling
				size = 334887
				religion = regent_court
			}
			create_pop = {
				culture = creek_gnome
				size = 418608
			}
		}
	}


	#Small Country
	s:STATE_ROYSFORT = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1348458
				split_religion = {
					redfoot_halfling = {
						corinite = 0.9
						regent_court = 0.1
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 6725
			}
		}
	}
	s:STATE_THOMSBRIDGE = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1493168
				split_religion = {
					redfoot_halfling = {
						corinite = 0.9
						regent_court = 0.1
					}
				}
			}
		}
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1115831
			}
			create_pop = {
				culture = redfoot_halfling
				size = 858145
				religion = regent_court
			}
			create_pop = {
				culture = ruby_dwarf
				size = 236028
			}
		}
	}
	s:STATE_CIDERFIELD = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1227759
			}
			create_pop = {
				culture = bluefoot_halfling
				size = 1187701
			}
			create_pop = {
				culture = reverian
				size = 64092
			}
			create_pop = {
				culture = creek_gnome
				size = 24034
			}
		}
	}
	s:STATE_HOMEHILLS = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1850165
				split_religion = { #Growing Ravelian community in Viswall
					redfoot_halfling = {
						ravelian = 0.1
						corinite = 0.9
					}
				}
			}
			create_pop = {
				culture = bluefoot_halfling
				size = 1441861
				split_religion = { #Growing Ravelian community in Viswall
					bluefoot_halfling = {
						ravelian = 0.1
						corinite = 0.9
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 176153
			}
			create_pop = { #Aelcandari
				culture = lorentish
				size = 144260
			}
			create_pop = {
				culture = silver_dwarf
				size = 48210
			}
			create_pop = {
				culture = gawedi
				size = 31893
			}
			create_pop = {
				culture = fomarati_halfling
				size = 15947
			}
		}
	}
	s:STATE_ELKMARCH = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 975199
			}
			#These pops will be moved to Uelaire in the event it gets split off
			create_pop = {
				culture = uelairey
				size = 520000
				split_religion = {
					uelairey = {
						ravelian = 0.65 #Mostly converted to Ravelian over course of the 18th century
						regent_court = 0.35 #Using as placeholder for Cult of Uelos
					}
				}
			}
			create_pop = {
				culture = bluefoot_halfling
				size = 323226
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.1
						corinite = 0.9
					}
				}
			}
			create_pop = {
				culture = beefoot_halfling
				size = 49171
			}
		}
	}
	s:STATE_BEEPECK = {
		region_state:A14 = {
			create_pop = {
				culture = beefoot_halfling
				size = 1179487
				split_religion = { #Still some Corinite peasants outside of Beepeck proper
					beefoot_halfling = {
						ravelian = 0.9
						corinite = 0.1
					}
				}
			}
			create_pop = {
				culture = uelairey
				size = 200146
			}
			create_pop = {
				culture = exwesser
				size = 117601
			}
			create_pop = {
				culture = creek_gnome
				size = 55990
			}
			create_pop = {
				culture = silver_dwarf
				size = 23973
			}
		}
	}
	s:STATE_DRAGONHILLS = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 187302
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.1
						corinite = 0.9
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 29158
			}
			create_pop = {
				culture = gawedi
				size = 420188
				split_religion = {
					gawedi = {
						ravelian = 0.25
						corinite = 0.75
					}
				}
			}
		}
	}
}