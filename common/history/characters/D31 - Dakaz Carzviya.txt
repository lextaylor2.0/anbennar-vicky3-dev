﻿CHARACTERS = {
	c:D31 ?= {
		create_character = {
			template = ruler_dak
			on_created = {
				add_character_role = general
				set_character_immortal = yes
			}
		}
	}
}
