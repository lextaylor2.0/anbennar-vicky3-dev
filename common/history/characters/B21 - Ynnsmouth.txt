﻿CHARACTERS = {
	c:B21 ?= {
		create_character = {
			first_name = Evn_enn
			last_name = Sorugalawe
			historical = yes
			ruler = yes
			noble = yes
			culture = boek
			age = 66
			interest_group = ig_landowners
			ig_leader = yes
			ideology = ideology_moderate
			traits = {
				innovative cautious
			}
		}
	}
}
