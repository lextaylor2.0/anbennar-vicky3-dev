﻿building_damestear_mine = {
	building_group = bg_damestear_mining
	icon = "gfx/interface/icons/building_icons/damestear_mine.dds"
	city_type = mine
	levels_per_mesh = 5
	required_construction = construction_cost_medium
	terrain_manipulator = mining
	
	unlocking_technologies = {
		shaft_mining # same as vanilla mines
	}

	production_method_groups = {
		pmg_mining_equipment_building_damestear_mine
		pmg_explosives_building_damestear_mine
		pmg_steam_automation_building_damestear_mine
		pmg_train_automation_building_damestear_mine
	}
	
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_mining.dds"
}

building_damestear_fields = {
	building_group = bg_damestear_fields
	icon = "gfx/interface/icons/building_icons/damestear_fields.dds"
	city_type = mine
	levels_per_mesh = 5
	buildable = no
	expandable = no
	downsizeable = no
	cannot_switch_owner = yes
	terrain_manipulator = mining
	
	unlocking_technologies = {
		prospecting
	}

	production_method_groups = {
		pmg_base_building_damestear_fields
	}

	ownership_type = self
	
	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_mining.dds"
}

building_flawless_metal_mine = {
	building_group = bg_flawless_metal_mining
	icon = "gfx/interface/icons/building_icons/mithril_mine.dds"
	city_type = mine
	levels_per_mesh = 5
	required_construction = construction_cost_medium
	terrain_manipulator = mining
	
	unlocking_technologies = {
		shaft_mining # same as vanilla mines
	}

	production_method_groups = {
		pmg_mining_equipment_building_flawless_metal_mine
		pmg_explosives_building_flawless_metal_mine
		pmg_forging_building_flawless_metal_mine
		pmg_automation_building_flawless_metal_mine
	}

	ownership_type = self
	
	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_mining.dds"
}


