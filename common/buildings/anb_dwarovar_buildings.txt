﻿
building_dwarven_hold = {
	building_group = bg_dwarven_hold
	city_type = city
	icon = "gfx/interface/icons/building_icons/skyscraper.dds"

	has_max_level = yes
	ignore_stateregion_max_level = yes

	unlocking_technologies = { urbanization }

	production_method_groups = {
		pmg_base_building_dwarven_hold
		pmg_transportation_building_dwarven_hold
		pmg_specialization_building_dwarven_hold
		pmg_automation_building_dwarven_hold
	}

	required_construction = construction_cost_hold
	downsizeable = no
	ownership_type = self
	cannot_switch_owner = yes

	ai_value = 2000 # Holds are infrastructure
	should_auto_expand = { 
		occupancy >= 0.8
		OR = {
			state = {
				sg:transportation = { 
					state_goods_pricier > 0.8
				}
			}
			state.market_access < 1
		}
		NOT = { is_under_construction = yes }
	}
	
	potential = {
		has_state_trait = state_trait_dwarven_hold
	}

	possible = {
		has_state_trait = state_trait_dwarven_hold
	}

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_monuments.dds"
}

building_dwarovrod = {
	building_group = bg_dwarovrod	
	icon = "gfx/interface/icons/building_icons/building_railway.dds"
	
	ai_nationalization_desire = 0.5 # AI is reluctant to privatize railways since they are likely to need subsidies anyway
	
	unlocking_technologies = {
		urban_planning
	}

	production_method_groups = {
		pmg_base_building_dwarovrod
		pmg_rails_building_dwarovrod
		pmg_automation_building_dwarovrod
	}

	required_construction = construction_cost_medium
	ownership_type = self

	ai_value = 2000 # Railways are important
	
	should_auto_expand = { 
		occupancy >= 0.8
		OR = {
			cash_reserves_ratio > 0.5
			is_subsidized = yes
		}
		OR = {
			state = {
				sg:transportation = { 
					state_goods_pricier > 0.5 
				}
			}
			state.market_access < 1
		}
		NOT = { is_under_construction = yes }
	}
	

	possible = {
		has_state_trait = state_trait_dwarovrod
	}

	potential = {
		has_state_trait = state_trait_dwarovrod
	}

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_railways.dds"
}


#1:1 copy of rice paddies, just diff name
building_subsistence_cavern_farm = {
	building_group = bg_subsistence_agriculture
	icon = "gfx/interface/icons/building_icons/building_subsistence_caverns.dds"

	production_method_groups = {
		pmg_base_building_subsistence_rice_paddies
		pmg_home_workshops_building_subsistence_rice_paddies
		pmg_serfdom_building_subsistence_rice_paddies
	}
	
	buildable = no
	expandable = no
	downsizeable = no
	min_raise_to_hire = 0.30
	slaves_role = peasants
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_farming.dds"
}



building_serpentbloom_farm = {
	building_group = bg_serpentbloom_farms
	
	icon = "gfx/interface/icons/building_icons/wheat_farm.dds"

	city_type = farm
	levels_per_mesh = 0

	unlocking_technologies = {
		enclosure
	}

	production_method_groups = {
		pmg_base_building_serpentbloom_farm
		pmg_secondary_building_serpentbloom_farm
		pmg_enhancements_farm
		pmg_harvesting_process_building_serpentbloom_farm
	}

	required_construction = construction_cost_low
	
	terrain_manipulator = farmland_wheat
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_farming.dds"
}

building_mushroom_farm = {
	building_group = bg_mushroom_farms
	
	icon = "gfx/interface/icons/building_icons/rye_farm.dds"

	city_type = none
	levels_per_mesh = 0

	unlocking_technologies = {
		enclosure
	}

	production_method_groups = {
		pmg_base_building_mushroom_farm
		pmg_secondary_building_mushroom_farm
		pmg_enhancements_farm
		pmg_harvesting_process_building_mushroom_farm
	}

	required_construction = construction_cost_low
	
	terrain_manipulator = farmland_rye
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_farming.dds"
}


building_cave_coral = {
	building_group = bg_cave_coral
	icon = "gfx/interface/icons/building_icons/logging_camp.dds"
	city_type = none
	required_construction = construction_cost_low
	terrain_manipulator = forestry
	levels_per_mesh = 0

	production_method_groups = {
		pmg_base_building_cave_coral
		pmg_hardwood_cave_coral
		pmg_equipment_cave_coral
		pmg_transportation_building_cave_coral
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_farming.dds"
}



building_mithril_mine = {
	building_group = bg_mithril_mining
	icon = "gfx/interface/icons/building_icons/mithril_mine.dds"
	city_type = mine
	levels_per_mesh = 0
	required_construction = construction_cost_medium
	terrain_manipulator = mining
	
	unlocking_technologies = {
		shaft_mining
	}

	production_method_groups = {
		pmg_mining_equipment_building_flawless_metal_mine
		pmg_explosives_building_flawless_metal_mine
		pmg_forging_building_flawless_metal_mine
		pmg_automation_building_flawless_metal_mine
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_mining.dds"
}

building_gem_mine = {
	building_group = bg_gem_mining
	icon = "gfx/interface/icons/building_icons/iron_mine.dds"
	city_type = mine
	levels_per_mesh = 5
	required_construction = construction_cost_medium
	terrain_manipulator = mining
	
	unlocking_technologies = {
		shaft_mining
	}

	production_method_groups = {
		pmg_mining_equipment_building_gem_mine
		pmg_explosives_building_gem_mine
		pmg_steam_automation_building_gem_mine
		pmg_train_automation_building_gem_mine
	}
	ownership_type = self

	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_mining.dds"
}