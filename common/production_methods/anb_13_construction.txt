﻿pm_conjured_frame_buildings = {
	texture = "gfx/interface/icons/production_method_icons/iron_frame_buildings.dds"
	
	unlocking_technologies = {
		urban_planning
	}

	country_modifiers = {
		workforce_scaled = {
			country_construction_add = 5 
		}

	}
	building_modifiers = {
		workforce_scaled = {
			goods_input_wood_add = 40
			goods_input_fabric_add = 20
			goods_input_iron_add = 50
			goods_input_tools_add = 10
			goods_input_magical_reagents_add = 30
		}

		level_scaled = {
			building_employment_bureaucrats_add = 500
			building_employment_clerks_add = 500
			building_employment_mages_add = 250
			building_employment_laborers_add = 3250
		}
		unscaled = {
			building_laborers_mortality_mult = 0.1
		}
	}
	
	state_modifiers = {
		workforce_scaled = {
			state_construction_mult = 0.004
		}	
	}
}

pm_mimic_precursor_steel_buildings = {
	texture = "gfx/interface/icons/production_method_icons/arc_welded_buildings.dds"

	unlocking_technologies = {
		mimic_precursor_steel
	}
		
	country_modifiers = {
		workforce_scaled = {
			country_construction_add = 20
		}
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_flawless_metal_add = 50
			goods_input_glass_add = 40
			goods_input_explosives_add = 20
			goods_input_tools_add = 40
			goods_input_electricity_add = 40
			goods_input_relics_add = 20
			goods_input_damestear_add = 30
		} 

		level_scaled = {
			building_employment_bureaucrats_add = 500
			building_employment_clerks_add = 500
			building_employment_engineers_add = 750
			building_employment_machinists_add = 1000
			building_employment_laborers_add = 2250
		}
		unscaled = {
			building_laborers_mortality_mult = 0.1
		}
	}
	
	state_modifiers = {
		workforce_scaled = {
			state_construction_mult = 0.015
		}	
	}

	required_input_goods = electricity
}

pm_builder_automata = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized
		law_sapience_mechanim_compromise
	}
	unlocking_technologies = {
		early_mechanim			
	}



	unlocking_production_methods = {
		pm_iron_frame_buildings
		pm_steel_frame_buildings
		pm_arc_welded_buildings
		pm_mimic_precursor_steel_buildings
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_arithmaton_engineering = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized
		law_sapience_mechanim_compromise
	}

	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_production_methods = {
		pm_steel_frame_buildings
		pm_arc_welded_buildings
		pm_mimic_precursor_steel_buildings
	}
    


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 12
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1000
			building_employment_clerks_add = -500
		}
	}
}

pm_builder_automata_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized_enforced
		law_sapience_mechanim_compromise_enforced
	}
	is_hidden_when_unavailable = yes
	unlocking_technologies = {
		early_mechanim			
	}

	unlocking_production_methods = {
		pm_iron_frame_buildings
		pm_steel_frame_buildings
		pm_arc_welded_buildings
		pm_mimic_precursor_steel_buildings
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 7
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_arithmaton_engineering_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_unrecognized_enforced
		law_sapience_mechanim_compromise_enforced
	}
	is_hidden_when_unavailable = yes
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_production_methods = {
		pm_steel_frame_buildings
		pm_arc_welded_buildings
		pm_mimic_precursor_steel_buildings
	}
    
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 13
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1000
			building_employment_clerks_add = -500
		}
	}
}