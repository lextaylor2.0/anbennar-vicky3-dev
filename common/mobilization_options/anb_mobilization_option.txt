##Infantry Weapons

mobilization_option_melee_weapon = {
	texture = "gfx/interface/icons/mobilization_options/meleeweapons.dds"
	can_be_turned_off = {
		custom_tooltip = {
		    text = mobilization_option_melee_weapon_can_be_turned_off_tt
			always = no
		}
	}
	upkeep_modifier = {
	}
	upkeep_modifier_unscaled = {
	}
	
	unit_modifier = {
		unit_offense_mult = -0.25
		unit_defense_mult = -0.25
		unit_kill_rate_add = -0.1
	}
	ai_weight = {
		value = 1
	}

	group = infantry_weapons
}

mobilization_option_firearms= {
	texture = "gfx/interface/icons/mobilization_options/forced_march.dds"

	possible = {
		market ?= {
			mg:small_arms ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
	}
	upkeep_modifier = {
		goods_input_small_arms_add = 0.5
	}
	unit_modifier = {
		unit_offense_mult = 0.25
		unit_defense_mult = 0.25
		unit_kill_rate_add = 0.1
	}
	ai_weight = {
		value = 1
	}

    group = infantry_weapons
}

mobilization_option_relic_imbuments = {
	texture = "gfx/interface/icons/mobilization_options/relicsimbuments.dds"

	possible = {
		market ?= {
			mg:relics ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
	}
	upkeep_modifier = {
		goods_input_relics_add = 1
	}
	unit_modifier = {
		unit_offense_add = 5
		unit_kill_rate_add = 0.03
	}
	ai_weight = {
		value = 1
	}

    group = infantry_weapons
}

mobilization_option_vorpal_coating = {
	texture = "gfx/interface/icons/mobilization_options/vorpal.dds"

	possible = {
		market ?= {
			mg:flawless_metal ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
	}
	upkeep_modifier = {
		goods_input_flawless_metal_add = 1
	}
	unit_modifier = {
		unit_offense_add = 5
		unit_kill_rate_add = 0.03
	}
	ai_weight = {
		value = 1
	}

    group = infantry_weapons
}

mobilization_option_black_damestear= {
	texture = "gfx/interface/icons/mobilization_options/blackdamestear.dds"

	possible = {
		market ?= {
			mg:damestear ?= {
				market_goods_sell_orders > 0
			}
			mg:ammunition ?= {
				market_goods_sell_orders > 0
			}
		}
		scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_firearms }
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
		elemental_elicitation
	}
	upkeep_modifier = {
		goods_input_ammunition_add = 0.5
		goods_input_damestear_add = 0.5
	}
	unit_modifier = {
		unit_offense_mult = 0.1
		unit_defense_mult = 0.1
	}
	ai_weight = {
		value = 1
	}

    group = infantry_weapons
}

mobilization_option_auto_reloading= {
	texture = "gfx/interface/icons/mobilization_options/machinegunners.dds"

	possible = {
		market ?= {
			mg:artificery_doodads ?= {
				market_goods_sell_orders > 0
			}
		}
		scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_firearms }
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
		automatic_machine_guns
	}
	upkeep_modifier = {
		goods_input_small_arms_add = 0.5
		goods_input_artificery_doodads_add = 0.5
	}
	unit_modifier = {
		unit_offense_add = 5
		unit_kill_rate_add = 0.03
	}
	ai_weight = {
		value = 1
	}

    group = infantry_weapons
}

mobilization_option_planar_phasing= {
	texture = "gfx/interface/icons/mobilization_options/phasing.dds"

	possible = {
		market ?= {
			mg:magical_reagents ?= {
				market_goods_sell_orders > 0
			}
			mg:ammunition ?= {
				market_goods_sell_orders > 0
			}
		}
		scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_firearms }
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
		planar_portals
	}
	upkeep_modifier = {
		goods_input_ammunition_add = 0.5
		goods_input_magical_reagents_add = 1
	}
	unit_modifier = {
		unit_offense_add = 5
		unit_kill_rate_add = 0.03
	}
	ai_weight = {
		value = 1
	}

    group = infantry_weapons
}

##Offensive armaments
mobilization_option_elemental_armaments = {
	texture = "gfx/interface/icons/mobilization_options/flamethrowers.dds"

	possible = {
		market ?= {
			mg:artificery_doodads ?= {
				market_goods_sell_orders > 0
			}
			mg:small_arms ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
		elemental_elicitation
	}
	upkeep_modifier = {
		goods_input_small_arms_add = 0.5
		goods_input_artificery_doodads_add = 0.5
	}
	unit_modifier = {
		unit_offense_add = 5
		unit_devastation_mult = 0.1
	}
	ai_weight = {
		value = 1
	}

    group = special_weapons
}

##Defensive Armaments
mobilization_option_bulletproof_armor = {
	texture = "gfx/interface/icons/mobilization_options/vorpal.dds"

	possible = {
		market ?= {
			mg:flawless_metal ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	upkeep_modifier = {
		goods_input_flawless_metal_add = 1
	}
	unit_modifier = {
		unit_defense_add = 5
		unit_recovery_rate_add = 0.05
	}
	ai_weight = {
		value = 1
	}

    group = defensive_weapons
}

mobilization_option_infantry_exosuits = {
	texture = "gfx/interface/icons/mobilization_options/vorpal.dds"

	possible = {
		market ?= {
			mg:artificery_doodads ?= {
				market_goods_sell_orders > 0
			}
			mg:steel ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	upkeep_modifier = {
		goods_input_artificery_doodads_add = 1
		goods_input_steel_add = 1
	}
	unit_modifier = {
		unit_defense_add = 10
		unit_recovery_rate_add = 0.05
	}
	ai_weight = {
		value = 1
	}
	
	unlocking_technologies = {
		thinking_servos
	}

    group = defensive_weapons
}

mobilization_option_warsuits = {
	texture = "gfx/interface/icons/mobilization_options/vorpal.dds"

	possible = {
		market ?= {
			mg:artificery_doodads ?= {
				market_goods_sell_orders > 0
			}
			mg:flawless_metal ?= {
				market_goods_sell_orders > 0
			}
			mg:rubber ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	upkeep_modifier = {
		goods_input_artificery_doodads_add = 1
		goods_input_flawless_metal_add = 1
		goods_input_rubber_add = 1
	}
	unit_modifier = {
		unit_defense_add = 15
		unit_recovery_rate_add = 0.1
	}
	ai_weight = {
		value = 1
	}
	
	unlocking_technologies = {
		mechs
	}

    group = defensive_weapons
}

mobilization_option_portable_turrets = {
	texture = "gfx/interface/icons/mobilization_options/machinegunners.dds"

	possible = {
		market ?= {
			mg:artificery_doodads= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	upkeep_modifier = {
		goods_input_artificery_doodads_add = 1
		goods_input_small_arms_add = 0.5
	}
	unit_modifier = {
		unit_defense_add = 5
		unit_kill_rate_add = 0.05
	}
	ai_weight = {
		value = 1
	}
	
	unlocking_technologies = {
		thinking_servos
	}

    group = defensive_weapons
}

mobilization_option_antimagic_field_generators = {
	texture = "gfx/interface/icons/mobilization_options/artificerstuff.dds"

	possible = {
		market ?= {
			mg:artificery_doodads?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	upkeep_modifier = {
		goods_input_artificery_doodads_add = 2
	}
	unit_modifier = {
		unit_defense_add = 10
	}
	ai_weight = {
		value = 1
	}
	
	unlocking_technologies = {
		psionic_theory
	}

    group = defensive_weapons
}

mobilization_option_triggered_mageshields = {
	texture = "gfx/interface/icons/mobilization_options/artificerstuff.dds"

	possible = {
		market ?= {
			mg:artificery_doodads?= {
				market_goods_sell_orders > 0
			}
		}
		scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_firearms }
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	upkeep_modifier = {
		goods_input_artificery_doodads_add = 1
	}
	unit_modifier = {
		unit_defense_add = 8
	}
	ai_weight = {
		value = 1
	}
	
	unlocking_technologies = {
		the_fifth_fundamental_force
	}

    group = defensive_weapons
}

#Supplements

mobilization_option_bloodrage_potion = {
	texture = "gfx/interface/icons/mobilization_options/potions.dds"

	possible = {
		market ?= {
			mg:magical_reagents ?= {
				market_goods_sell_orders > 0
			}
		}
		NOT = { scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_alchemical_capsule } }
	}

	on_deactivate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_morale_when_you_remove_supplies_while_in_combat_tt
			every_combat_unit = {
				add_morale = {
					value = morale
					multiply = -0.5
				}
			}
		}
	}

	upkeep_modifier = {
		goods_input_magical_reagents_add = 1
	}
	unit_modifier = {
		unit_morale_recovery_mult = 0.1
		unit_offense_add = 3
	}
	ai_weight = {
		value = 1
	}
	
	unlocking_technologies = {
		fractional_distillation
	}

	group = supplements
}

mobilization_option_alchemical_capsule = {
	texture = "gfx/interface/icons/mobilization_options/pills.dds"

	possible = {
		market ?= {
			mg:magical_reagents ?= {
				market_goods_sell_orders > 0
			}
		}
		NOT = { scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_bloodrage_potion } }
	}

	on_deactivate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_morale_when_you_remove_supplies_while_in_combat_tt
			every_combat_unit = {
				add_morale = {
					value = morale
					multiply = -0.5
				}
			}
		}
	}

	upkeep_modifier = {
		goods_input_magical_reagents_add = 3
	}
	unit_modifier = {
		unit_morale_recovery_mult = 0.2
		unit_offense_add = 6
	}
	ai_weight = {
		value = 1
	}
	
	unlocking_technologies = {
		artificial_life
	}

	group = supplements
}

#Medical
mobilization_option_brass_prosthesis = {
	texture = "gfx/interface/icons/mobilization_options/artificerstuff.dds"

	unlocking_technologies = {
		brass_prosthesis
	}
	possible = {
		NOT = { scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_limb_regeneration } }
		NOT = { scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_biological_transmutation } }
		market ?= {
			mg:iron ?= {
				market_goods_sell_orders > 0
			}
			mg:artificery_doodads ?= {
				market_goods_sell_orders > 0
			}
			mg:lead ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_to_add_medic_support_tt
			add_organization = {
				value = organization
				multiply = -0.25
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_morale_and_organization_when_you_remove_medic_support_tt
			every_combat_unit = {
				add_morale = {
					value = morale
					multiply = -0.5
				}
			}
			add_organization = {
				value = organization
				multiply = -0.25
			}
		}
	}
	upkeep_modifier = {
		goods_input_iron_add = 0.5
		goods_input_artificery_doodads_add = 0.5
		goods_input_lead_add = 0.5
	}
	unit_modifier = {
		unit_recovery_rate_add = 0.10	
	}
	ai_weight = {
		value = 1
	}

    group = medic_support
}

mobilization_option_limb_regeneration = {
	texture = "gfx/interface/icons/mobilization_options/artificerstuff.dds"

	unlocking_technologies = {
		gene_transmutation
	}
	possible = {
		NOT = { scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_brass_prosthesis } }
		NOT = { scope:military_formation = { has_mobilization_option = mobilization_option:mobilization_option_biological_transmutation } }
		market ?= {
			mg:steel ?= {
				market_goods_sell_orders > 0
			}
			mg:artificery_doodads ?= {
				market_goods_sell_orders > 0
			}
			mg:lead ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_to_add_medic_support_tt
			add_organization = {
				value = organization
				multiply = -0.25
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_morale_and_organization_when_you_remove_medic_support_tt
			every_combat_unit = {
				add_morale = {
					value = morale
					multiply = -0.5
				}
			}
			add_organization = {
				value = organization
				multiply = -0.25
			}
		}
	}
	upkeep_modifier = {
		goods_input_steel_add = 1
		goods_input_artificery_doodads_add = 1
		goods_input_lead_add = 1
	}
	unit_modifier = {
		unit_recovery_rate_add = 0.15	
		unit_offense_add = 6
	}
	ai_weight = {
		value = 1
	}

    group = medic_support
}

#Transport

#Utilities

mobilization_option_scrying_disruptors= {
	texture = "gfx/interface/icons/mobilization_options/artificerstuff.dds"

	possible = {
		market ?= {
			mg:artificery_doodads ?= {
				market_goods_sell_orders > 0
			}
			mg:porcelain ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
		magic_wave_stabilization
	}
	upkeep_modifier = {
		goods_input_artificery_doodads_add = 0.5
		goods_input_porcelain_add = 1
	}
	unit_modifier = {
		character_battle_condition_surprise_maneuver_mult = 0.1
		unit_defense_add = 5
	}
	ai_weight = {
		value = 1
	}

    group = reconnaissance
}

mobilization_option_automata_support= {
	texture = "gfx/interface/icons/mobilization_options/artificerstuff.dds"

	possible = {
		market ?= {
			mg:automata ?= {
				market_goods_sell_orders > 0
			}
			mg:small_arms ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
		early_mechanim
	}
	upkeep_modifier = {
		goods_input_automata_add = 1
		goods_input_small_arms_add = 1
	}
	unit_modifier = {
		unit_kill_rate_add = 0.1
		unit_defense_add = 10
	}
	ai_weight = {
		value = 1
	}

    group = reconnaissance
}

mobilization_option_t_hacking_transceivers {
	texture = "gfx/interface/icons/mobilization_options/artificerstuff.dds"

	possible = {
		market ?= {
			mg:radios ?= {
				market_goods_sell_orders > 0
			}
		}
	}
	
	on_activate_while_mobilized = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	on_deactivate = {
		custom_tooltip = {
			text = mobilization_option_it_hurts_organization_when_you_adjust_equipment_tt
			add_organization = {
				value = organization
				multiply = -0.5
			}
		}
	}
	
	unlocking_technologies = {
		apparitional_communicators
	}
	upkeep_modifier = {
		goods_input_radios_add = 2
	}
	unit_modifier = {
		character_battle_condition_surprise_maneuver_mult = 0.5
		character_battle_condition_lost_mult = -0.5
	}
	ai_weight = {
		value = 1
	}

    group = reconnaissance
}